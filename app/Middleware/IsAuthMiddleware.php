<?php

namespace App\Middleware;

use App\Core\Base\InterfaceMiddleware;
use App\Core\Exceptions\UnauthorizedException;
use App\Services\AuthService;

/**
 * Class IsAuthMiddleware
 * @package App\Middleware
 */
class IsAuthMiddleware implements InterfaceMiddleware
{
    /**
     * @throws UnauthorizedException
     */
    public static function initialize(): void
    {
        if (!AuthService::isAuthorized()) {
            throw new UnauthorizedException();
        }
    }
}
