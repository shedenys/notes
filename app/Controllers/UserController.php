<?php

namespace App\Controllers;

use App\Core\Base\AbstractController;
use App\Core\Exceptions\ValidationException;
use App\Core\FlashMessage;
use App\Core\Request;
use App\Services\AuthService;
use App\Services\UserService;
use App\Validators\UserUpdatePasswordValidator;

/**
 * Class UserController
 * @package App\Controllers
 */
class UserController extends AbstractController
{
    /**
     * @var AuthService
     */
    protected $authService;

    /**
     * @var UserService
     */
    protected $userService;

    public function __construct()
    {
        $this->authService = new AuthService();
        $this->userService = new UserService();
    }

    /**
     * Update password
     * @throws \App\Core\Exceptions\ModelExecuteException
     */
    public function currentUpdatePassword(): void
    {
        try {
            UserUpdatePasswordValidator::validate(Request::all());
        } catch (ValidationException $e) {
            FlashMessage::sendError($e->getMessage(), UserUpdatePasswordValidator::$errors);
            Request::redirectBack();
        }
        if ($this->userService->updatePassword(Request::get('current_password'), Request::get('new_password'))) {
            FlashMessage::sendSuccess('Your password successfully updated!');
        } else {
            FlashMessage::sendError('Your current password is invalid!');
        }
        Request::redirect('/auth/profile-view');
    }

    /**
     * Delete current user
     * @throws \App\Core\Exceptions\ModelExecuteException
     * @throws \App\Core\Exceptions\NotFoundException
     */
    public function currentDelete(): void
    {
        $this->userService->delete();
        FlashMessage::sendSuccess('Your account successfully deleted!');
        Request::redirect('/');
    }
}
