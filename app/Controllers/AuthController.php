<?php

namespace App\Controllers;

use App\Core\Base\AbstractController;
use App\Core\Exceptions\ValidationException;
use App\Core\FlashMessage;
use App\Core\Request;
use App\Services\AuthService;
use App\Validators\AuthLoginValidator;
use App\Validators\AuthRegisterValidator;

/**
 * Class AuthController
 * @package App\Controllers
 */
class AuthController extends AbstractController
{
    /**
     * @var AuthService
     */
    protected $authService;

    public function __construct()
    {
        $this->authService = new AuthService();
    }

    /**
     * Register page
     * @return string
     */
    public function registerView(): string
    {
        $title = 'Register';

        return $this->view('register', compact('title'));
    }

    /**
     * Login page
     * @return string
     */
    public function loginView(): string
    {
        $title = 'Login';

        return $this->view('login', compact('title'));
    }

    /**
     * Profile page
     * @return string
     */
    public function profileView(): string
    {
        $title = 'Profile';

        return $this->view('profile', compact('title'));
    }

    /**
     * Create user
     * @throws \App\Core\Exceptions\ModelExecuteException
     */
    public function register(): void
    {
        try {
            AuthRegisterValidator::validate(Request::all());
        } catch (ValidationException $e) {
            FlashMessage::sendError($e->getMessage(), AuthRegisterValidator::$errors);
            Request::redirectBack();
        }

        $this->authService->register(Request::get('login'), Request::get('password'));
        FlashMessage::sendSuccess('You are successfully registered!');
        Request::redirect('/');
    }


    /**
     * User authorization
     */
    public function login(): void
    {
        try {
            AuthLoginValidator::validate(Request::all());
        } catch (ValidationException $e) {
            FlashMessage::sendError($e->getMessage(), AuthLoginValidator::$errors);
            Request::redirectBack();
        }
        if ($this->authService->login(Request::get('login'), Request::get('password'))) {
            FlashMessage::sendSuccess('You are successfully authorized!');
            Request::redirect('/auth/profile-view');
        }
        FlashMessage::sendError('Invalid login and/or password!');
        Request::redirect('/auth/login-view');
    }

    /**
     * User logout
     */
    public function logout(): void
    {
        $this->authService->logout();
        FlashMessage::sendSuccess('You are successfully logout!');
        Request::redirect('/');
    }
}
