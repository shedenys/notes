<?php

namespace App\Controllers;

use App\Core\Base\AbstractController;

/**
 * Index controller
 * Class IndexController
 * @package App\Controllers
 */
class IndexController extends AbstractController
{
    /**
     * Index action
     * @return string
     */
    public function index(): string
    {
//        $model = new User();
//        $model->save([
//            'name' => 'first',
//            'email' => 'first@mail.com',
//            'password' => 'first',
//            'remember_token' => 'first'
//        ]);
//        $model->name = 'new name';
//        $model->save();
//
//        $foundModel = User::findOrFail($model->id);
//        echo $foundModel->name;

        $title = 'Home';

        return $this->view('index', compact('title'));
    }

    /**
     * Edit action
     * @param string $param1
     * @return string
     */
    public function edit(string $param1, string $param2): string
    {
        return $this->view(
            'index',
            [
                'title' => $param1,
                'text' => $param2
            ]
        );
    }
}
