<?php

namespace App\Models;

use App\Core\Model;

/**
 * Class User
 * @package App\Models
 */
class User extends Model
{
    protected static $table = 'users';

    public $id;

    public $login;

    public $password;

    public $remember_token;
}
