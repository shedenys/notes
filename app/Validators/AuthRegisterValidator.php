<?php

namespace App\Validators;

use App\Core\Base\AbstractValidator;
use App\Repositories\UserRepository;

class AuthRegisterValidator extends AbstractValidator
{
    /**
     * Validation
     * @param array $data
     * @return mixed|void
     * @throws \App\Core\Exceptions\ValidationException
     */
    public static function validate(array $data)
    {
        if (empty($data['login'])) {
            static::addError('login', self::$requiredErrorMessage);
        }
        if (empty($data['password'])) {
            static::addError('password', self::$requiredErrorMessage);
        }
        if (empty($data['repeat_password'])) {
            static::addError('repeat_password', self::$requiredErrorMessage);
        }
        if (!empty($data['password'])
            && !empty($data['repeat_password'])
            && $data['password'] !== $data['repeat_password']
        ) {
            static::addError('repeat_password', 'Must be equal password!');
        }
        $userRepository = new UserRepository();
        if ($userRepository->doesUserExist($data['login'])) {
            static::addError('login', 'This login is already taken');
        }
        self::checkErrors();
    }
}
