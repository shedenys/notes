<?php

namespace App\Validators;

use App\Core\Base\AbstractValidator;

/**
 * Class AuthLoginValidator
 * @package App\Validators
 */
class AuthLoginValidator extends AbstractValidator
{
    /**
     * Validation
     * @param array $data
     * @return mixed|void
     * @throws \App\Core\Exceptions\ValidationException
     */
    public static function validate(array $data)
    {
        if (empty($data['login'])) {
            static::addError('login', self::$requiredErrorMessage);
        }
        if (empty($data['password'])) {
            static::addError('password', self::$requiredErrorMessage);
        }
        self::checkErrors();
    }
}
