<?php

namespace App\Validators;

use App\Core\Base\AbstractValidator;

/**
 * Validation on user update password action
 * Class AuthUpdatePasswordValidator
 * @package App\Validators
 */
class UserUpdatePasswordValidator extends AbstractValidator
{
    /**
     * Validation
     * @param array $data
     * @return mixed|void
     * @throws \App\Core\Exceptions\ValidationException
     */
    public static function validate(array $data)
    {
        if (empty($data['current_password'])) {
            static::addError('current_password', self::$requiredErrorMessage);
        }
        if (empty($data['new_password'])) {
            static::addError('new_password', self::$requiredErrorMessage);
        }
        if (isset($data['current_password'], $data['new_password'])
            && ($data['current_password'] === $data['new_password'])) {
            static::addError('new_password', 'New password must not be equal to current');
        }
        self::checkErrors();
    }
}
