<?php

try {
    session_start();
    require_once 'Core/Autoloader.php';
    $loader = new Autoloader();
    $loader->register();
    $loader->addNamespace('App', __DIR__);
    \App\Core\Config::initialize(include __DIR__.'/../config.php');
    require_once __DIR__.'/routes.php';
    \App\Core\Route::initialize();
} catch (Throwable $e) {
    \App\Core\Response::send($e->getMessage(), $e->getCode());
}
