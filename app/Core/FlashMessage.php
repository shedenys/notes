<?php

namespace App\Core;

/**
 * Flash messages
 * Class FlashMessage
 * @package App\Core
 */
class FlashMessage
{
    protected const SESSION_KEY = 'flash_message';

    protected const STATUS_KEY = 'status';

    protected const MESSAGE_KEY = 'message';

    protected const ERRORS_KEY = 'errors';

    protected const TYPE_SUCCESS = 'success';

    protected const TYPE_ERROR = 'danger';

    protected const TYPE_DEFAULT = 'info';

    /**
     * @var string
     */
    protected static $type;

    /**
     * @var array
     */
    protected static $errors;

    /**
     * Get last message and clear
     * @return mixed
     */
    public static function get()
    {
        $flashMessage = Session::get(static::SESSION_KEY);
        Session::delete(static::SESSION_KEY);
        static::$type = $flashMessage[static::STATUS_KEY];
        if (isset($flashMessage[static::ERRORS_KEY])) {
            static::$errors = $flashMessage[static::ERRORS_KEY];
        }

        return $flashMessage[static::MESSAGE_KEY];
    }

    /**
     * Get message type
     * @return mixed
     */
    public static function getType()
    {
        return static::$type;
    }

    /**
     * Send simple message
     * @param string $message
     */
    public static function send(string $message): void
    {
        Session::set(static::SESSION_KEY, [
            static::STATUS_KEY => static::TYPE_DEFAULT,
            static::MESSAGE_KEY => $message
        ]);
    }

    /**
     * Send success message
     * @param string $message
     */
    public static function sendSuccess(string $message): void
    {
        Session::set(static::SESSION_KEY, [
            static::STATUS_KEY => static::TYPE_SUCCESS,
            static::MESSAGE_KEY => $message
        ]);
    }

    /**
     * Send error message
     * @param string $message
     * @param array $errors
     */
    public static function sendError(string $message, array $errors = []): void
    {
        Session::set(static::SESSION_KEY, [
            static::STATUS_KEY => static::TYPE_ERROR,
            static::MESSAGE_KEY => $message,
            static::ERRORS_KEY => $errors,
        ]);
    }

    /**
     * Return error value
     * @param string $errorKey
     * @return string|null
     */
    public static function getError(string $errorKey): ?string
    {
        return static::$errors[$errorKey] ?? null;
    }
}
