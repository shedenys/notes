<?php

namespace App\Core;

/**
 * For working with $_SERVER
 * Class Server
 * @package App\Core
 */
class Server
{
    /**
     * All values of $_SERVER
     * @return array
     */
    public static function all(): array
    {
        return $_SERVER;
    }

    /**
     * Get value from $_SERVER
     * @param $key
     * @return mixed
     */
    public static function get($key)
    {
        return $_SERVER[$key]; // this still trigger a phpmd warning
    }
}
