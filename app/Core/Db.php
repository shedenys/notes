<?php

namespace App\Core;

use PDO;

/**
 * Class Db
 * @package App\Core
 */
class Db
{
    /**
     * @var PDO
     */
    private static $db;

    /**
     * @return PDO
     */
    public static function getDb(): PDO
    {
        if (self::$db === null) {
            self::$db = new PDO(
                'mysql:host=' . Config::get('DB_HOST') . ';port=' . Config::get('DB_PORT') . ';dbname='
                . Config::get('DB_NAME') . ';charset=utf8',
                Config::get('DB_USER'),
                Config::get('DB_PASSWORD')
            );
        }

        return self::$db;
    }

    private function __clone()
    {
    }

    /**
     * Db constructor
     */
    private function __construct()
    {
    }
}
