<?php

namespace App\Core\Base;

use App\Core\View;

/**
 * Abstract controller
 * Class AbstractController
 * @package App\Controllers
 */
abstract class AbstractController
{
    /**
     * View render method
     * @param string $view
     * @param array $params
     * @return string
     */
    protected function view(string $view, array $params = []): string
    {
        return View::render($view, $params);
    }
}
