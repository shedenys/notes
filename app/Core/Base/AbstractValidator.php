<?php

namespace App\Core\Base;

use App\Core\Exceptions\ValidationException;

/**
 * Validator
 * Class AbstractValidator
 * @package App\Core
 */
abstract class AbstractValidator
{
    /**
     * @var string
     */
    protected static $requiredErrorMessage = 'Field is required!';

    /**
     * Error messages
     * @var array
     */
    public static $errors = [];

    /**
     * Validation body
     * @param array $data
     * @return mixed
     */
    abstract public static function validate(array $data);

    /**
     * Add error message
     * @param string $attribute
     * @param string $message
     */
    protected static function addError(string $attribute, string $message): void
    {
        static::$errors[$attribute] = $message;
    }

    /**
     * Check error messages
     * @throws ValidationException
     */
    protected static function checkErrors(): void
    {
        if (static::$errors) {
            throw new ValidationException();
        }
    }
}
