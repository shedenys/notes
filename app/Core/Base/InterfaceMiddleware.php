<?php

namespace App\Core\Base;

/**
 * Interface InterfaceMiddleware
 * @package App\Core\Base
 */
interface InterfaceMiddleware
{
    /**
     * Initialize middleware
     */
    public static function initialize(): void;
}
