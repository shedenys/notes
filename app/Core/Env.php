<?php

namespace App\Core;

/**
 * Encapsulating access to $_ENV
 * Class Env
 * @package App\Core
 */
class Env
{
    /**
     * Get $_ENV
     * @param string $key
     * @return string
     */
    public static function get(string $key): string
    {
        if (empty(isset($_ENV[$key]))) { // this still trigger a phpmd warning
            throw new \LogicException("$key is empty or not defined in config.php");
        }
        return $_ENV[$key]; // this still trigger a phpmd warning
    }

    /**
     * Set param to $_ENV
     * @param string $key
     * @param string $value
     */
    public static function set(string $key, string $value): void
    {
        $_ENV[$key] = $value; // this still trigger a phpmd warning
    }
}
