<?php

namespace App\Core;

/**
 * Class Response
 * @package App\Core
 */
class Response
{
    /**
     * @param string $message
     * @param int $code
     */
    public static function send(string $message, int $code): void
    {
        http_response_code($code);
        ob_clean();
        ob_start();
        echo $message;
        echo ob_get_clean();
    }
}