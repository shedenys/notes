<?php

namespace App\Core;

/**
 * For working with $_GET
 * Class Input
 * @package App\Core
 */
class Input
{
    /**
     * All values of $_GET
     * @return array
     */
    public static function all(): array
    {
        return $_GET; // this still trigger a phpmd warning
    }

    /**
     * Get value from $_GET
     * @param $key
     * @return mixed
     */
    public static function get($key)
    {
        return $_GET[$key]; // this still trigger a phpmd warning
    }
}
