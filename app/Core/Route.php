<?php

namespace App\Core;

use App\Core\Base\InterfaceMiddleware;
use App\Core\Exceptions\MethodNotAllowedException;
use App\Core\Exceptions\NotFoundException;

/**
 * Class Route
 * @package App\Core
 */
class Route
{
    protected const ROUTE_CONTROLLER_PARAM = 'controller';

    protected const ROUTE_CONTROLLER_METHOD_PARAM = 'controller_method';

    protected const ROUTE_REQUEST_METHOD_PARAM = 'request_method';

    protected const ROUTE_MIDDLEWARE_PARAM = 'middleware';

    /**
     * Routes list
     * @var array
     */
    public static $routes = [];

    /**
     * Add route with any method
     * @param string $route
     * @param string $controller
     * @param string $method
     * @param string|null $middleware
     */
    public static function any(string $route, string $controller, ?string $method, ?string $middleware = null): void
    {
        self::add($route, $controller, $method, null, $middleware);
    }

    /**
     * Add route with only GET method
     * @param string $route
     * @param string $controller
     * @param string $method
     * @param string|null $middleware
     */
    public static function get(string $route, string $controller, string $method, ?string $middleware = null): void
    {
        self::add($route, $controller, $method, 'GET', $middleware);
    }

    /**
     * Add route with only POST method
     * @param string $route
     * @param string $controller
     * @param string $method
     * @param string|null $middleware
     */
    public static function post(string $route, string $controller, string $method, ?string $middleware = null): void
    {
        self::add($route, $controller, $method, 'POST', $middleware);
    }

    /**
     * Add route with only PUT method
     * @param string $route
     * @param string $controller
     * @param string $method
     * @param string $middleware
     */
    public static function put(string $route, string $controller, string $method, ?string $middleware = null): void
    {
        self::add($route, $controller, $method, 'PUT', $middleware);
    }

    /**
     * Add route with only DELETE method
     * @param string $route
     * @param string $controller
     * @param string $method
     * @param string $middleware
     */
    public static function delete(string $route, string $controller, string $method, ?string $middleware = null): void
    {
        self::add($route, $controller, $method, 'DELETE', $middleware);
    }

    /**
     * Initialization and searching current route
     * @throws NotFoundException
     * @throws MethodNotAllowedException
     */
    public static function initialize(): void
    {
        $uriParts = explode('?', Server::get('REQUEST_URI'), 2);
        $route = $uriParts[0];
        if (!self::$routes[$route]) {
            throw new NotFoundException();
        }
        if (self::$routes[$route][self::ROUTE_REQUEST_METHOD_PARAM]
            && self::$routes[$route][self::ROUTE_REQUEST_METHOD_PARAM]
            !== Server::get('REQUEST_METHOD')
        ) {
            throw new MethodNotAllowedException();
        }
        $middleware = self::$routes[$route][self::ROUTE_MIDDLEWARE_PARAM];
        if ($middleware) {
            /** @var InterfaceMiddleware $middleware */
            $middleware::initialize();
        }
        $controller = new self::$routes[$route][self::ROUTE_CONTROLLER_PARAM];
        $method = self::$routes[$route][self::ROUTE_CONTROLLER_METHOD_PARAM];
        echo $controller->$method();
    }

    /**
     * Add route
     * @param string $route
     * @param string $controller
     * @param string $controllerMethod
     * @param string $requestMethod
     * @param string $middleware
     */
    protected static function add(
        string $route,
        string $controller,
        string $controllerMethod,
        ?string $requestMethod,
        ?string $middleware
    ): void {
        self::$routes[$route] = [
            self::ROUTE_CONTROLLER_PARAM => $controller,
            self::ROUTE_CONTROLLER_METHOD_PARAM => $controllerMethod,
            self::ROUTE_REQUEST_METHOD_PARAM => $requestMethod,
            self::ROUTE_MIDDLEWARE_PARAM => $middleware
        ];
    }
}
