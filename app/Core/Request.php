<?php

namespace App\Core;

class Request
{
    /**
     * All values of $_POST
     * @return array
     */
    public static function all(): array
    {
        return $_POST; // this still trigger a phpmd warning
    }

    /**
     * Get value from $_POST
     * @param $key
     * @return mixed
     */
    public static function get($key)
    {
        return $_POST[$key]; // this still trigger a phpmd warning
    }

    /**
     * Redirect
     * @param string $location
     * @param int $statusCode
     */
    public static function redirect(string $location, int $statusCode = 303): void
    {
        header('Location: ' . $location, true, $statusCode);
        die;
    }

    /**
     * Redirect back
     * @param int $statusCode
     */
    public static function redirectBack(int $statusCode = 303): void
    {
        static::redirect(Server::get('HTTP_REFERER'));
    }
}
