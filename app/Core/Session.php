<?php

namespace App\Core;


class Session
{
    /**
     * Get value from $_SESSION
     * @param $key
     * @return mixed
     */
    public static function get($key)
    {
        return $_SESSION[$key] ?? null;
    }

    /**
     * Set value to $_SESSION
     * @param string $key
     * @param mixed $value
     */
    public static function set(string $key, $value): void
    {
        $_SESSION[$key] = $value; // this still trigger a phpmd warning
    }

    /**
     * Delete value from $_SESSION
     * @param $key
     * @return mixed
     */
    public static function delete($key): void
    {
        unset($_SESSION[$key]); // this still trigger a phpmd warning
    }
}
