<?php

namespace App\Core;

use App\Core\Exceptions\ModelExecuteException;
use App\Core\Exceptions\NotFoundException;
use PDO;

/**
 * Class Model
 * @package App\Core
 */
class Model
{
    /**
     * @var \PDO
     */
    protected $db;

    /**
     * @var string
     */
    protected static $table;

    /**
     * Model constructor.
     */
    public function __construct()
    {
        $this->db = Db::getDb();
    }

    /**
     * Get table name
     * @return string
     */
    public static function getTableName(): string
    {
        return static::$table;
    }

    /**
     * Get first model
     * Model::first(['id', 'status'], 'status = :status', ['status' => 1]);
     * Model::first([], 'status = :status', ['status' => 1], ['status' => Db::getDb()::PARAM_INT]);
     * @param array $columns
     * @param string|null $whereConditions
     * @param array $bindParams
     * @param array $bindTypes
     * @return mixed
     */
    public static function first(
        array $columns = [],
        string $whereConditions = null,
        array $bindParams = [],
        array $bindTypes = []
    ) {
        return self::select($columns, $whereConditions, $bindParams, $bindTypes)->fetch();
    }

    /**
     * Get the model by ID
     * Model::find(1, ['id', 'name']);
     * @param int $id
     * @param array $columns
     * @return mixed
     */
    public static function find(int $id, array $columns = [])
    {
        return self::select($columns, 'id = :id', [':id' => $id], ['id' => Db::getDb()::PARAM_INT])
            ->fetch();
    }

    /**
     * Get the model by ID or throw an exception
     * Model::findOrFail(1, ['id', 'name']);
     * @param int $id
     * @param array $columns
     * @return mixed
     * @throws NotFoundException
     */
    public static function findOrFail(int $id, array $columns = [])
    {
        $model = self::find($id, $columns);
        if (!$model) {
            throw new NotFoundException();
        }

        return $model;
    }

    /**
     * Return array of models
     * Model::get(['id', 'name'])
     * @param array $columns
     * @param string|null $whereConditions
     * @param array $bindParams
     * @param array $bindTypes
     * @return array
     */
    public static function get(
        array $columns = [],
        string $whereConditions = null,
        array $bindParams = [],
        array $bindTypes = []
    ): array {
         return self::select($columns, $whereConditions, $bindParams, $bindTypes)->fetchAll();
    }

    /**
     * Create or update the model
     * $model->save([
     *     'name' => 'Name',
     *     'last_name' => 'Last Name',
     * ]);
     * @param array $data
     * @return void
     * @throws ModelExecuteException
     */
    public function save(array $data = []): void
    {
        if ($this->{'id'}) {
            $this->update();
            return;
        }
        if ($data) {
            foreach ($data as $column => $value) {
                $this->$column = $value;
            }
        }
        $modelColumns = $this->getModelProperties(['id']);
        $modelColumnBinds = [];
        foreach ($modelColumns as $column) {
            $modelColumnBinds[$column] = ":$column";
        }
        $statement = $this->db->prepare('
            INSERT INTO ' . static::$table . ' 
                (' . implode(', ', $modelColumns) . ') 
            VALUES 
                (' . implode(', ', $modelColumnBinds) . ')
        ');
        foreach ($modelColumnBinds as $column => $bindIndex) {
            $statement->bindParam($bindIndex, $this->$column);
        }
        if (!$statement->execute()) {
            $this->getError($statement);
        }
        $this->{'id'} = $this->db->lastInsertId();
    }

    /**
     * Update model
     * @return void
     * @throws ModelExecuteException
     */
    public function update(): void
    {
        if (!isset($this->id)) {
            return;
        }
        $modelColumns = $this->getModelProperties(['id']);
        $modelColumnBinds = [];
        $set = '';
        $count = count($modelColumns);
        $i = 1;
        foreach ($modelColumns as $column) {
            $modelColumnBinds[$column] = ":$column";
            $set .= $column . ' = ' . ":$column";
            if ($i < $count) {
                $set .= ', ';
            }
            $i++;
        }
        /** @noinspection SqlResolve */
        $statement = $this->db->prepare('UPDATE ' . static::$table . " SET $set WHERE id = :id");
        foreach ($modelColumnBinds as $column => $bindIndex) {
            $statement->bindParam($bindIndex, $this->$column);
        }
        $statement->bindParam(':id', $this->id);
        if (!$statement->execute()) {
            $this->getError($statement);
        }
    }

    /**
     * Delete model
     * @return void
     * @throws ModelExecuteException
     * @throws NotFoundException
     */
    public function delete(): void
    {
        if (!isset($this->id)) {
            throw new NotFoundException();
        }
        /** @noinspection SqlResolve */
        $statement = $this->db->prepare('DELETE FROM ' . static::$table . ' WHERE id = :id');
        $statement->bindParam(':id', $this->id);
        if (!$statement->execute()) {
            $this->getError($statement);
        }
    }

    /**
     * Return model properties (columns)
     * @param array $exclude
     * @return array
     */
    protected function getModelProperties(array $exclude = []): array
    {
        return array_diff(array_keys(get_object_vars($this)), array_merge(['table', 'db'], $exclude));
    }

    /**
     * Build of select statement
     * @param array $columns
     * @param string|null $whereConditions
     * @param array $bindParams
     * @param array $bindTypes
     * @return \PDOStatement
     */
    private static function select(
        array $columns = [],
        string $whereConditions = null,
        array $bindParams = [],
        array $bindTypes = []
    ): \PDOStatement {

        if (!count($columns)) {
            $sqlColumns = '*';
        } else {
            $sqlColumns = implode(', ', $columns);
        }
        $sql = "SELECT $sqlColumns FROM " . static::$table;
        if ($whereConditions) {
            $sql .= ' WHERE ' . $whereConditions;
        }
        $statement = Db::getDb()->prepare($sql);
        foreach ($bindParams as $key => $value) {
            if (!empty($bindTypes[$key])) {
                $statement->bindParam($key, $value, $bindTypes[$key]);
            } else {
                $statement->bindParam($key, $value);
            }
        }
        $statement->execute();
        if (!count($columns)) {
            $statement->setFetchMode(PDO::FETCH_CLASS, static::class);
        } else {
            $statement->setFetchMode(PDO::FETCH_ASSOC);
        }

        return $statement;
    }

    /**
     * Get error message
     * @param \PDOStatement $statement
     * @throws ModelExecuteException
     */
    private function getError(\PDOStatement $statement): void
    {
        throw new ModelExecuteException($statement->errorInfo()[2]);
    }
}
