<?php

namespace App\Core\Exceptions;

/**
 * Exception for case, when model not found
 * Class ModelNotFoundException
 * @package App\Core\Exceptions
 */
class NotFoundException extends \Exception
{
    /**
     * ModelNotFoundException constructor.
     * @param string $message
     * @param int $code
     */
    public function __construct(string $message = 'Page not found', int $code = 404)
    {
        parent::__construct($message, $code);
    }
}
