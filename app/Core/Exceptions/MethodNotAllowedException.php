<?php

namespace App\Core\Exceptions;

class MethodNotAllowedException extends \Exception
{
    /**
     * ModelNotFoundException constructor.
     * @param string $message
     * @param int $code
     */
    public function __construct(string $message = 'Method not allowed', int $code = 405)
    {
        parent::__construct($message, $code);
    }
}