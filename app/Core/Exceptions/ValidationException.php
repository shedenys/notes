<?php

namespace App\Core\Exceptions;

class ValidationException extends \Exception
{
    public function __construct(string $message = 'Validation error', int $code = 422)
    {
        parent::__construct($message, $code);
    }
}
