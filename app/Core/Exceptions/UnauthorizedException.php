<?php

namespace App\Core\Exceptions;

/**
 * Class UnauthorizedException
 * @package App\Core\Exceptions
 */
class UnauthorizedException extends \Exception
{
    public function __construct(string $message = 'You are unauthorized', int $code = 401)
    {
        parent::__construct($message, $code);
    }
}
