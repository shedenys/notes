<?php

namespace App\Core\Exceptions;

/**
 * Exception for case, when model query execution failed
 * Class ModelExecuteException
 */
class ModelExecuteException extends \Exception
{
    /**
     * ModelExecuteException constructor.
     * @param string $message
     * @param int $code
     */
    public function __construct(string $message = 'Model query execute failed', int $code = 423)
    {
        parent::__construct($message, $code);
    }
}
