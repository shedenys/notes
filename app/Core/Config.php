<?php

namespace App\Core;

/**
 * Class Config
 * @package App\Core
 */
class Config
{
    public const ENV_TESTING_PREFIX = 'TEST';

    public const ENV_PARAM = 'APP_ENV';

    public const ENV_PRODUCTION = 'production';

    public const ENV_TESTING = 'testing';

    public const ENV_DEVELOPMENT = 'development';

    /**
     * Initialize config values
     * @param array $config
     */
    public static function initialize(array $config): void
    {
        foreach ($config as $key => $value) {
            // If param is not for testing environment
            if (strpos($key, self::ENV_TESTING_PREFIX . '_') !== 0) {
                // If is set testing environment and param has testing variant
                if ($config[self::ENV_PARAM] === self::ENV_TESTING && $config[self::ENV_TESTING_PREFIX . "_$key"]) {
                    // Use value for testing environment
                    $value = $config[self::ENV_TESTING_PREFIX . "_$key"];
                }
                Env::set($key, $value);
            }
        }
    }

    /**
     * Get param value
     * @param string $key
     * @return string
     */
    public static function get(string $key): string
    {
        return Env::get($key);
    }
}
