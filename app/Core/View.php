<?php

namespace App\Core;

/**
 * Class View
 * @package App\Core
 */
class View
{
    /**
     * Render view
     * @param string $view
     * @param array $params
     * @return string
     */
    public static function render(string $view, array $params): string
    {
        extract($params, EXTR_OVERWRITE);
        ob_start();
        /** @noinspection PhpIncludeInspection  */
        // TODO implement multi-layout view rendering
        require_once __DIR__. '/../Views/layout.phtml';

        return ob_get_clean();
    }
}
