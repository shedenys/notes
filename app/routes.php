<?php

use App\Core\Route;

Route::any('/', \App\Controllers\IndexController::class, 'index');
Route::any('/auth/register-view', \App\Controllers\AuthController::class, 'registerView');
Route::post('/auth/register', \App\Controllers\AuthController::class, 'register');
Route::get('/auth/login-view', \App\Controllers\AuthController::class, 'loginView');
Route::post('/auth/login', \App\Controllers\AuthController::class, 'login');
Route::get(
    '/auth/profile-view',
    \App\Controllers\AuthController::class,
    'profileView',
    \App\Middleware\IsAuthMiddleware::class
);
Route::get(
    '/auth/logout',
    \App\Controllers\AuthController::class,
    'logout',
    \App\Middleware\IsAuthMiddleware::class
);
Route::post(
    '/user/current/update-password',
    \App\Controllers\UserController::class,
    'currentUpdatePassword',
    \App\Middleware\IsAuthMiddleware::class
);
Route::get(
    '/user/current/delete',
    \App\Controllers\UserController::class,
    'currentDelete',
    \App\Middleware\IsAuthMiddleware::class
);
