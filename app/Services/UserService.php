<?php

namespace App\Services;

use App\Repositories\UserRepository;

/**
 * Class UserService
 * @package App\Services
 */
class UserService
{
    /**
     * @var AuthService
     */
    protected $authService;

    /**
     * @var UserRepository
     */
    protected $userRepository;

    public function __construct()
    {
        $this->authService = new AuthService();
        $this->userRepository = new UserRepository();
    }

    /**
     * Update password for current user
     * @param string $currentPassword
     * @param string $newPassword
     * @return bool
     * @throws \App\Core\Exceptions\ModelExecuteException
     */
    public function updatePassword(string $currentPassword, string $newPassword): bool
    {
        $user = $this->userRepository->find(AuthService::currentUser()['id']);
        if (!password_verify($currentPassword, $user->password)) {
            return false;
        }
        $this->userRepository->updatePassword($user, $newPassword);

        return true;
    }

    /**
     * Delete current user
     * @throws \App\Core\Exceptions\ModelExecuteException
     * @throws \App\Core\Exceptions\NotFoundException
     */
    public function delete(): void
    {
        $user = $this->userRepository->find(AuthService::currentUser()['id']);
        $user->delete();
        $this->authService->logout();
    }
}
