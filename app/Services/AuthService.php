<?php

namespace App\Services;

use App\Core\Session;
use App\Models\User;
use App\Repositories\UserRepository;

/**
 * Class AuthRepository
 */
class AuthService
{
    public const SESSION_AUTH_KEY = 'auth';

    public const SESSION_AUTH_USER_KEY = 'user';

    /**
     * @var UserRepository
     */
    protected $userRepository;

    /**
     * AuthService constructor.
     */
    public function __construct()
    {
        $this->userRepository = new UserRepository();
    }

    /**
     * Register user
     * @param string $login
     * @param string $password
     * @throws \App\Core\Exceptions\ModelExecuteException
     */
    public function register(string $login, string $password): void
    {
        $this->userRepository->create($login, $password);
    }

    /**
     * User login
     * @param string $login
     * @param string $password
     * @return bool
     */
    public function login(string $login, string $password): bool
    {
        $user = User::first([], 'login = :login', [
            'login' => $login
        ]);
        if (password_verify($password, $user->password)) {
            Session::set(self::SESSION_AUTH_KEY, [
                self::SESSION_AUTH_USER_KEY => get_object_vars($user)
            ]);

            return true;
        }

        return false;
    }

    /**
     * User logout
     */
    public function logout(): void
    {
        Session::delete(self::SESSION_AUTH_KEY);
    }

    /**
     * Get auth data
     * @return array
     */
    protected static function get(): ?array
    {
        return Session::get(self::SESSION_AUTH_KEY);
    }

    /**
     * Get auth data
     * @return array
     */
    public static function currentUser(): ?array
    {
        return static::get()[self::SESSION_AUTH_USER_KEY];
    }

    /**
     * Check if user authorized
     * @return bool
     */
    public static function isAuthorized(): bool
    {
        if (static::currentUser()) {
            return true;
        }

        return false;
    }
}
