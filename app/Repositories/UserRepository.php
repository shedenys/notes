<?php

namespace App\Repositories;

use App\Models\User;

/**
 * For operating with User model only
 * Class UserRepository
 * @package App\Repositories
 */
class UserRepository
{
    /**
     * Create user
     * @param string $login
     * @param string $password
     * @return User
     * @throws \App\Core\Exceptions\ModelExecuteException
     */
    public function create(string $login, string $password): User
    {
        $user = new User();
        $user->login = $login;
        $user->password = password_hash($password, PASSWORD_DEFAULT);
        $user->save();

        return $user;
    }

    /**
     * Update password
     * @param User $user
     * @param string $newPassword
     * @throws \App\Core\Exceptions\ModelExecuteException
     */
    public function updatePassword(User $user, string $newPassword): void
    {
        $user->password = password_hash($newPassword, PASSWORD_DEFAULT);
        $user->save();
    }

    /**
     * Find user
     * @param int $userId
     * @return User
     */
    public function find(int $userId): User
    {
        return User::find($userId);
    }

    /**
     * Does user exist by login
     * @param string $login
     * @return bool
     */
    public function doesUserExist(string $login): bool
    {
        return (bool) User::first(['id'], 'login = :login', ['login' => $login]);
    }
}
