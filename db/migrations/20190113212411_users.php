<?php

use Phinx\Db\Adapter\MysqlAdapter;
use Phinx\Migration\AbstractMigration;

/**
 * Migration for Users table
 */
class Users extends AbstractMigration
{
    public function change(): void
    {
        $table = $this->table('users', [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci',
        ]);
        $table->addColumn('login', 'string', [
            'limit' => MysqlAdapter::TEXT_SMALL,
            'comment' => 'login',
        ])
        ->addColumn('password', 'string', [
            'limit' => MysqlAdapter::TEXT_SMALL,
            'comment' => 'Password',
        ])
        ->addColumn('remember_token', 'string', [
            'limit' => MysqlAdapter::TEXT_SMALL,
            'comment' => 'Remember Token',
            'null' => true
        ])
        ->addIndex(['login'], ['unique' => true])
        ->create();
    }
}
