<?php

use App\Core\Config;

require_once 'app/bootstrap.php';

return [
    'paths' => [
        'migrations' => '%%PHINX_CONFIG_DIR%%/db/migrations',
        'seeds' => '%%PHINX_CONFIG_DIR%%/db/seeds',
    ],
    'environments' => [
        'default_database' => Config::get('APP_ENV'),
        'production' => [
            'adapter' => 'mysql',
            'host' => Config::get('DB_HOST'),
            'name' => Config::get('DB_NAME'),
            'user' => Config::get('DB_USER'),
            'pass' => Config::get('DB_PASSWORD'),
            'port' => Config::get('DB_PORT'),
            'charset' => 'utf8',
        ],
        'development' => [
            'adapter' => 'mysql',
            'host' => Config::get('DB_HOST'),
            'name' => Config::get('DB_NAME'),
            'user' => Config::get('DB_USER'),
            'pass' => Config::get('DB_PASSWORD'),
            'port' => Config::get('DB_PORT'),
            'charset' => 'utf8',
        ],
        'testing' => [
            'adapter' => 'mysql',
            'host' => Config::get('DB_HOST'),
            'name' => Config::get('DB_NAME'),
            'user' => Config::get('DB_USER'),
            'pass' => Config::get('DB_PASSWORD'),
            'port' => Config::get('DB_PORT'),
            'charset' => 'utf8',
        ]
    ],
    'version_order' => 'creation'
];
